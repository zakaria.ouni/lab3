#include <stdlib.h>
#include <time.h>
#include "jeu.h"

int comparaison(char Joueur , char pc) 
{
    if (Joueur == pc) {
        return 0 ; }
    else if ((Joueur == 'R' && pc == 'C') ||
               (Joueur == 'P' && pc == 'R') ||
               (Joueur == 'C' && pc == 'P')) 
        {
         return 1 ; 
        } 
    else 
    {
         return -1 ; 
    }
}
char hasard() {
    srand(time(NULL));
    int nombreAleatoire = rand() % 3;
    if (nombreAleatoire == 0) {
        return 'R';
    } else if (nombreAleatoire == 1) {
        return 'P';
    } else {
        return 'C';
    }
}
